#import "CommonWidgetPlugin.h"
#if __has_include(<common_widget/common_widget-Swift.h>)
#import <common_widget/common_widget-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "common_widget-Swift.h"
#endif

@implementation CommonWidgetPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftCommonWidgetPlugin registerWithRegistrar:registrar];
}
@end
