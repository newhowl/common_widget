
import 'dart:async';

import 'package:flutter/services.dart';

class CommonWidget {
  static const MethodChannel _channel = MethodChannel('common_widget');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
