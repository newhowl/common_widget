import 'package:common_util/constant/color_constant.dart';
import 'package:common_util/constant/size_constant.dart';
import 'package:common_util/constant/text_size_constant.dart';
import 'package:common_util/util/object_util.dart';
import 'package:common_widget/widget/asset_safe_image/asset_safe_image.dart';
import 'package:common_widget/widget/button/ink_well_button.dart';
import 'package:common_widget/widget/container/round_container.dart';
import 'package:common_widget/widget/edge/edge_inset.dart';
import 'package:common_widget/widget/network_image/network_image_view.dart';
import 'package:common_widget/widget/text/rich_text.dart';
import 'package:flutter/material.dart';

import 'appbar_constant.dart';

PreferredSizeWidget appBar({
  Color backgroundColor = colorWhite,
  Widget? leading,
  Widget? title,
  double titleSpacingOrg = appbarTitleWithoutLeadingSpacing,
  bool? centerTitle,
  List<Widget>? actions,
  double? elevation,
  Color shadowColor = colorClear,
  PreferredSizeWidget? bottom,
}) {
  return AppBar(
    backgroundColor: backgroundColor,
    leading: leading,
    leadingWidth: isExists(leading) ? null : sizePadding16,
    title: title,
    titleSpacing: titleSpacingOrg,
    centerTitle: centerTitle,
    actions: actions,
    elevation: elevation,
    shadowColor: shadowColor,
    bottom: bottom ?? appBarBottomLine(),
  );
}

Widget appBarLeading({
  required BuildContext context,
  required String asset,
  Color? color,
  double paddingSizeOrg = appbarLeadingPaddingSize,
  VoidCallback? onTap,
}) {
  return InkWellButton(
    onTap: onTap ?? () => Navigator.of(context).pop(),
    child: RoundContainer(
      padding: edgeAllScreenUtil(paddingSizeOrg),
      alignment: Alignment.center,
      child: AssetSafeImage(
        asset: asset,
        color: color,
        width: sizeIcon24,
        height: sizeIcon24,
      ),
    ),
  );
}

Widget appBarTitle({
  required String title,
  double sizeOrg = textSize14,
  Color color = colorBlack,
  FontWeight fontWeight = FontWeight.normal,
}) {
  return richTextOfString(
    title,
    size: sizeOrg,
    color: color,
    fontWeight: fontWeight,
  );
}

Widget appBarTitleWithAsset({
  required String asset,
  double? width,
  double height = 24.0,
}) {
  return AssetSafeImage(asset: asset, width: width, height: height, fit: BoxFit.fitHeight,);
}

Widget appBarTitleWithNetworkImage({
  required String imageUrl,
  required String title,
  double imageUrlSizeOrg = appbarTitleWithImageUrlSize,
  double textSizeOrg = textSize14,
  FontWeight fontWeight = FontWeight.normal,
  Color color = colorBlack,
  GestureTapCallback? onTap,
}) {
  return InkWellButton(
    onTap: onTap,
    child: Center(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(width: sizeGap12,),
          RoundContainer(
            roundSize: imageUrlSizeOrg,
            child: NetworkImageView(
              imageUrl: imageUrl,
              width: imageUrlSizeOrg,
              height: imageUrlSizeOrg,
              fit: BoxFit.cover,
            ),
          ),
          const SizedBox(width: sizeGap4,),
          Flexible(child: richTextOfString(title, size: textSizeOrg, fontWeight: fontWeight)),
          const SizedBox(width: sizeGap12,),
        ],
      ),
    ),
  );
}

Widget appBarTitleWithNetworkImageWithAsset({
  required String imageUrl,
  required String title,
  required String asset,
  double imageUrlSizeOrg = appbarTitleWithImageUrlSize,
  double textSizeOrg = textSize14,
  FontWeight fontWeight = FontWeight.normal,
  Color color = colorBlack,
  Color? borderColor,
  GestureTapCallback? onTap,
}) {
  return InkWellButton(
    onTap: onTap,
    child: Center(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(width: sizeGap6,),
          RoundContainer(
            roundSize: imageUrlSizeOrg,
            borderColor: borderColor,
            child: NetworkImageView(
              imageUrl: imageUrl,
              width: imageUrlSizeOrg,
              height: imageUrlSizeOrg,
              fit: BoxFit.cover,
            ),
          ),
          const SizedBox(width: sizeGap4,),
          Flexible(child: richTextOfString(title, size: textSizeOrg, fontWeight: fontWeight)),
          const SizedBox(width: sizeGap4,),
          AssetSafeImage(
            asset: asset,
            width: sizeIcon16,
            height: sizeIcon16,
          ),
          const SizedBox(width: sizeGap6,),
        ],
      ),
    ),
  );
}

List<Widget> appBarActionWithSizedBox({
  double widthOrg = appbarTitleWithImageUrlSize,
  double heightOrg = appbarTitleWithImageUrlSize,
}) {
  return [
    SizedBox(
      width: widthOrg,
      height: heightOrg,
    ),
  ];
}

PreferredSizeWidget appBarBottomLine() {
  return PreferredSize(
    preferredSize: const Size.fromHeight(1.0),
    child: Container(
      height: 1.0,
      color: const Color(0xFFF2F2F2),
    ),
  );
}
