const double appbarTitleWithoutLeadingSpacing = 12.0;
const double appBarTitleWithLeadingSpacing = 0.0;
const double appbarTitleWithImageUrlSize = 32.0;
const double appbarLeadingPaddingSize = 8.0;
