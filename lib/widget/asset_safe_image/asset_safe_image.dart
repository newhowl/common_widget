import 'package:common_util/util/string_util.dart';
import 'package:flutter/cupertino.dart';

class AssetSafeImage extends StatefulWidget {
  const AssetSafeImage({
    Key? key,
    required this.asset,
    this.padding,
    this.width,
    this.height,
    this.color,
    this.fit,
    this.alignment = Alignment.center,
    this.gaplessPlayback = false,
  }) : super(key: key);

  final String asset;
  final EdgeInsets? padding;
  final double? width;
  final double? height;
  final Color? color;
  final BoxFit? fit;
  final AlignmentGeometry alignment;
  final bool gaplessPlayback;

  @override
  _AssetSafeImageState createState() => _AssetSafeImageState();
}

class _AssetSafeImageState extends State<AssetSafeImage> {
  @override
  Widget build(BuildContext context) {
    if (isNotExistsStr(widget.asset)) {
      return Container();
    }
    return Container(
      padding: widget.padding,
      width: widget.width,
      height: widget.height,
      child: Image.asset(
        widget.asset,
        color: widget.color,
        fit: widget.fit,
        alignment: widget.alignment,
        gaplessPlayback: widget.gaplessPlayback,
      ),
    );
  }
}

