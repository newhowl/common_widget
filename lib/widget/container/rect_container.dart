import 'package:common_util/util/object_util.dart';
import 'package:common_widget/widget/button/ink_well_button.dart';
import 'package:flutter/material.dart';

class RectContainer extends StatelessWidget {
  const RectContainer({
    Key? key,
    this.constraints,
    this.alignment,
    this.padding,
    this.color,
    this.decoration,
    this.borderSize = 32.0,
    this.borderWidth = 0.5,
    this.borderColor,
    // this.foregroundDecoration,
    this.widthOrg,
    this.heightOrg,
    this.childWidthOrg,
    this.childHeightOrg,
    this.margin,
    this.transform,
    this.transformAlignment,
    this.child,
    this.clipBehavior = Clip.none,
    this.onTap,
  }) : super(key: key);

  final BoxConstraints? constraints;
  final AlignmentGeometry? alignment;
  final EdgeInsetsGeometry? padding;
  final Color? color;
  final Decoration? decoration;
  final double borderSize;
  final double borderWidth;
  final Color? borderColor;
  // final Decoration? foregroundDecoration;
  final double? widthOrg;
  final double? heightOrg;
  final double? childWidthOrg;
  final double? childHeightOrg;
  final Widget? child;
  final EdgeInsetsGeometry? margin;
  final Matrix4? transform;
  final AlignmentGeometry? transformAlignment;
  final Clip clipBehavior;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: InkWellButton(
        onTap: onTap,
        child: Container(
          constraints: constraints,
          key: key,
          alignment: alignment,
          padding: padding,
          color: isExists(borderColor) ? null : color,
          decoration: _decoration(),
          foregroundDecoration: _foregroundDecoration(),
          width: isExists(widthOrg) ? widthOrg : null,
          height: isExists(heightOrg) ? heightOrg : null,
          child: child,
          transform: transform,
          transformAlignment: transformAlignment,
          clipBehavior: clipBehavior,
        ),
      ),
    );
  }

  BoxDecoration? _decoration() {
    var isDecoration = isExists(borderColor);
    return isDecoration ? _existsDecoration() : null;
  }

  BoxDecoration _existsDecoration() {
    return BoxDecoration(
      color: color,
    );
  }

  BoxDecoration? _foregroundDecoration() {
    var isDecoration = isExists(borderColor);
    return isDecoration ? _existsForegroundDecoration() : null;
  }

  BoxDecoration _existsForegroundDecoration() {
    return BoxDecoration(
      border: Border.all(color: borderColor!, width: borderWidth),
    );
  }
}
