import 'package:flutter/cupertino.dart';

/*
 * 화면 해상도에 따라 변하는 EdgeInsets
 */
EdgeInsets edgeAllScreenUtil(double valueOrg) {
  return EdgeInsets.symmetric(
    vertical: valueOrg,
    horizontal: valueOrg,
  );
}

EdgeInsets edgeOnlyScreenUtil({
  double topOrg = 0.0,
  double bottomOrg = 0.0,
  double leftOrg = 0.0,
  double rightOrg = 0.0,
}) {
  return EdgeInsets.only(
    top: topOrg,
    bottom: bottomOrg,
    left: leftOrg,
    right: rightOrg,
  );
}

EdgeInsets edgeSymmetricScreenUtil({
  double verticalOrg = 0.0,
  double horizontalOrg = 0.0,
}) {
  return EdgeInsets.symmetric(
    vertical: verticalOrg,
    horizontal: horizontalOrg,
  );
}
