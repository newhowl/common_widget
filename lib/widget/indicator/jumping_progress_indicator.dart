import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';

const List<Color> colorJumpingDot = [
  Color(0xFF9bb1ff),
  Color(0xFF5a7bef),
  Color(0xFF254acd),
];

class _JumpingDot extends AnimatedWidget {
  const _JumpingDot({
    Key? key,
    required Animation<double> animation,
    required this.color,
    required this.fontSize
  }) : super(key: key, listenable: animation);

  final Color color;
  final double fontSize;

  @override
  Widget build(BuildContext context) {
    final Animation<double> animation = listenable as Animation<double>;
    return Container(
      alignment: Alignment.bottomCenter,
      height: animation.value,
      child: Icon(
        Icons.lens,
        color: color,
        size: fontSize,
      ),
    );
  }
}

class JumpingProgressIndicator extends StatefulWidget {
  const JumpingProgressIndicator({
    Key? key,
    this.numberOfDots = 3,
    this.dotSize = 10.0,
    this.colors = colorJumpingDot,
    this.dotSpacing = 2.0,
    this.milliseconds = 200,
  }) : super(key: key);

  final int numberOfDots;
  final double dotSize;
  final double dotSpacing;
  final List<Color> colors;
  final int milliseconds;
  final double beginTweenValue = 0.0;
  final double endTweenValue = 8.0;

  @override
  _JumpingProgressIndicatorState createState() => _JumpingProgressIndicatorState();
}

class _JumpingProgressIndicatorState extends State<JumpingProgressIndicator> with TickerProviderStateMixin {
  List<AnimationController> controllers = <AnimationController>[];
  List<Animation<double>> animations = <Animation<double>>[];
  final _widgets = <Widget>[];

  @override
  void initState() {
    super.initState();
    for (var i = 0; i < widget.numberOfDots; i++) {
      _addAnimationControllers();
      _buildAnimations(i);
      _addListOfDots(i);
    }
    controllers[0].forward();
  }

  void _addAnimationControllers() {
    controllers.add(AnimationController(duration: Duration(milliseconds: widget.milliseconds), vsync: this));
  }

  void _addListOfDots(int index) {
    _widgets.add(
      Padding(
        padding: EdgeInsets.only(right: widget.dotSpacing, left: widget.dotSpacing),
        child: _JumpingDot(
          animation: animations[index],
          fontSize: widget.dotSize,
          color: widget.colors[index],
        ),
      ),
    );
  }

  void _buildAnimations(int index) {
    animations.add(
      Tween(begin: widget.beginTweenValue, end: widget.endTweenValue).animate(controllers[index])
        ..addStatusListener(
              (AnimationStatus status) {
            if (status == AnimationStatus.completed) controllers[index].reverse();
            if (index == widget.numberOfDots - 1 && status == AnimationStatus.dismissed) {
              controllers[0].forward();
            }
            if (animations[index].value > widget.endTweenValue / 2 && index < widget.numberOfDots - 1) {
              controllers[index + 1].forward();
            }
          },
        ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.dotSize + (widget.dotSize * 0.5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: _widgets,
      ),
    );
  }

  @override
  void dispose() {
    for (var i = 0; i < widget.numberOfDots; i++) {
      controllers[i].dispose();
    }
    super.dispose();
  }
}
