import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

const double loadingCircleIndicatorBackgroundSize = 100.0;
const double loadingCircleIndicatorIconSize = 80.0;

class LoadingCircleIndicator extends StatefulWidget {
  const LoadingCircleIndicator({
    Key? key,
    this.isRefresh = false,
  }) : super(key: key);

  final bool isRefresh;

  @override
  _LoadingCircleIndicatorState createState() => _LoadingCircleIndicatorState();
}

class _LoadingCircleIndicatorState extends State<LoadingCircleIndicator> with TickerProviderStateMixin {
  int frame = 1;
  late Timer timer;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(const Duration(milliseconds: 100), timerCallback);
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
          width: widget.isRefresh
              ? loadingCircleIndicatorBackgroundSize * 0.8
              : loadingCircleIndicatorBackgroundSize,
          height: widget.isRefresh
              ? loadingCircleIndicatorBackgroundSize * 0.8
              : loadingCircleIndicatorBackgroundSize,
          child: Center(
            child: Image.asset(
              'assets/icon/loading_page/loading_$frame.png',
              width: widget.isRefresh
                  ? loadingCircleIndicatorIconSize * 0.8
                  : loadingCircleIndicatorIconSize,
              height: widget.isRefresh
                  ? loadingCircleIndicatorIconSize * 0.8
                  : loadingCircleIndicatorIconSize,
              gaplessPlayback: true,
            ),
          )),
    );
  }

  void timerCallback(Timer timer) {
    frame++;
    if (frame > 6) frame = 1;
    setState(() {});
  }
}
