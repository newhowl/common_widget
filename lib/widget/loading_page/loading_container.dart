import 'package:common_util/constant/color_constant.dart';
import 'package:common_util/loading_state/loading_state.dart';
import 'package:common_util/util/object_util.dart';
import 'package:common_widget/widget/edge/edge_inset.dart';
import 'package:common_widget/widget/indicator/jumping_progress_indicator.dart';
import 'package:flutter/material.dart';

class LoadingContainer extends StatefulWidget {
  const LoadingContainer({
    Key? key,
    required this.loadingState,
    this.isProgressIndicate = true,
    this.paddingTopProgressIndicate = 0.0,
    required this.child,
  }) : super(key: key);

  final LoadingState loadingState;
  final bool isProgressIndicate;
  final double paddingTopProgressIndicate;
  final Widget child;

  @override
  _LoadingContainerState createState() => _LoadingContainerState();
}

class _LoadingContainerState extends State<LoadingContainer> {
  @override
  Widget build(BuildContext context) => Stack(
        children: <Widget>[
          _getContentSection(),
          _progressIndicator(),
        ],
      );

  Widget _getContentSection() {
    return isExists(widget.child) ? widget.child : Container();
  }

  Widget _progressIndicator() {
    final isLoading = widget.loadingState.checkLoading();
    return isLoading && widget.isProgressIndicate
        ? Container(
            padding: edgeOnlyScreenUtil(topOrg: widget.paddingTopProgressIndicate),
            color: colorClearBlack060,
            child: const Center(
              child: JumpingProgressIndicator(),
            ),
          )
        : Container();
  }
}
