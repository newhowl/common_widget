import 'package:common_util/constant/color_constant.dart';
import 'package:common_util/util/media_util.dart';
import 'package:common_widget/widget/indicator/jumping_progress_indicator.dart';
import 'package:flutter/cupertino.dart';

class LoadingPage extends StatelessWidget {
  const LoadingPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _body(context);
  }

  Widget _body(context) {
    var width = widthMedia(context);
    var height = heightMedia(context);
    return Container(
      width: width,
      height: height,
      color: colorClearBlack030,
      child: const JumpingProgressIndicator(),
    );
  }
}