/// List
const double moreListLoadingTopPadding = 16.0;
const double moreListFirstTopPadding = 200.0;
const double moreListFooterSize = 16.0;

/// Grid
const int moreGridCrossAxisCount = 4;
const double moreGridChildAspectRatio = 0.8;

/// Empty





