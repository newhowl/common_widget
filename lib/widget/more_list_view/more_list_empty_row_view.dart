import 'package:common_util/constant/color_constant.dart';
import 'package:common_util/constant/size_constant.dart';
import 'package:common_util/constant/text_size_constant.dart';
import 'package:common_util/util/media_util.dart';
import 'package:common_widget/widget/asset_safe_image/asset_safe_image.dart';
import 'package:common_widget/widget/container/round_container.dart';
import 'package:common_widget/widget/text/rich_text.dart';
import 'package:flutter/cupertino.dart';

class MoreListEmptyRowView extends StatelessWidget {
  const MoreListEmptyRowView({
    Key? key,
    this.height,
    this.iconWidthOrg = sizeIcon48,
    this.iconHeightOrg = sizeIcon48,
    this.asset = '',
    this.title = '',
    this.description = '',
    this.baseColor = colorClear,
  }) : super(key: key);

  final double? height;
  final double iconWidthOrg;
  final double iconHeightOrg;
  final String asset;
  final String title;
  final String description;
  final Color baseColor;

  @override
  Widget build(BuildContext context) {
    return _body(context);
  }

  Widget _body(context) {
    var width = widthMedia(context);
    return Container(
      width: width,
      height: height,
      alignment: Alignment.center,
      child: _column(context),
    );
  }

  Widget _column(context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _icon(context),
        _title(context),
        _description(context),
      ],
    );
  }

  Widget _icon(context) {
    return RoundContainer(
      color: baseColor,
      roundSize: iconWidthOrg,
      alignment: Alignment.center,
      child: AssetSafeImage(
        asset: asset,
        width: iconWidthOrg,
        height: iconHeightOrg,
        color: colorGreyBDBDBD,
      ),
    );
  }

  Widget _title(context) {
    return richTextOfString(title, size: textSize13, color: colorGreyBDBDBD,);
  }

  Widget _description(context) {
    return richTextOfString(description, size: textSize13, color: colorGreyBDBDBD,);
  }
}