import 'package:common_util/constant/color_constant.dart';
import 'package:common_util/constant/size_constant.dart';
import 'package:common_util/constant/text_size_constant.dart';
import 'package:common_util/util/media_util.dart';
import 'package:common_util/util/string_util.dart';
import 'package:common_widget/widget/asset_safe_image/asset_safe_image.dart';
import 'package:common_widget/widget/container/round_container.dart';
import 'package:common_widget/widget/edge/edge_inset.dart';
import 'package:common_widget/widget/text/rich_text.dart';
import 'package:flutter/cupertino.dart';

class MoreListErrorView extends StatelessWidget {
  const MoreListErrorView({
    Key? key,
    this.iconWidthOrg = sizeIcon48,
    this.iconHeightOrg = sizeIcon48,
    this.asset = '',
    this.title = '',
    this.description = '',
    this.button = '',
    this.baseColor = colorClear,
    this.buttonCallback,
    this.topPaddingPer = 0.4,
    this.bottomPaddingPer = 0.6,
    this.topPadding = 100.0,
    this.bottomPadding = 100.0,
  }) : super(key: key);

  final double iconWidthOrg;
  final double iconHeightOrg;
  final String asset;
  final String title;
  final String description;
  final String button;
  final Color baseColor;
  final VoidCallback? buttonCallback;
  final double topPaddingPer;
  final double bottomPaddingPer;
  final double topPadding;
  final double bottomPadding;

  @override
  Widget build(BuildContext context) {
    return _body(context);
  }

  Widget _body(BuildContext context) {
    return Container(
      padding: edgeOnlyScreenUtil(topOrg: topPadding, bottomOrg: bottomPadding),
      child: _column(context),
    );
  }

  Widget _column(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _icon(context),
        _title(context),
        _description(context),
        _button(context),
      ],
    );
  }

  Widget _icon(BuildContext context) {
    return RoundContainer(
      color: baseColor,
      roundSize: iconWidthOrg,
      padding: edgeOnlyScreenUtil(bottomOrg: sizePadding16),
      alignment: Alignment.center,
      child: AssetSafeImage(
        asset: asset,
        width: iconWidthOrg,
        height: iconHeightOrg,
        color: colorGreyBDBDBD,
      ),
    );
  }

  Widget _title(BuildContext context) {
    var isShow = isExistsStr(title);
    if (!isShow) return Container();

    return Container(
      padding: edgeOnlyScreenUtil(bottomOrg: sizePadding10),
      child: richTextOfString(title, size: textSize13, color: colorRed,),
    );
  }

  Widget _description(BuildContext context) {
    var isShow = isExistsStr(description);
    if (!isShow) return Container();

    return Container(
      padding: edgeOnlyScreenUtil(bottomOrg: sizePadding10),
      child: richTextOfString(description, size: textSize13, color: colorRed,),
    );
  }

  Widget _button(BuildContext context) {
    var isShow = isExistsStr(button);
    if (!isShow) return Container();

    return RoundContainer(
      onTap: () {
        buttonCallback?.call();
      },
      margin: edgeSymmetricScreenUtil(verticalOrg: sizePadding10),
      padding: edgeSymmetricScreenUtil(verticalOrg: sizePadding8, horizontalOrg: sizePadding16),
      roundSize: sizeBorder4,
      color: colorBlack202020,
      child: richTextOfString(button, size: textSize14, color: colorWhite,),
    );
  }
}