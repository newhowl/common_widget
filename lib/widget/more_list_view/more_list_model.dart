import 'package:common_util/loading_state/loading_state.dart';
import 'package:common_util/util/int_util.dart';
import 'package:common_util/util/object_util.dart';
import 'package:common_widget/widget/more_list_view/top_scroll_model.dart';
import 'package:flutter/material.dart';

typedef MoreListWidgetBuilder = Widget Function(BuildContext context, int index, dynamic item);
typedef AddCallback = dynamic Function(Object object);

class MoreListModel {
  var loadingState = LoadingState.init();
  var topScroll = TopScroll();
  VoidCallback? refreshCallback;
  var list = <dynamic>[];
  dynamic lastSnapshot;

  void dispose() {
    topScroll.dispose();
    refreshCallback = null;
    list.clear();
    lastSnapshot = null;
  }

  ///
  /// By Docs
  ///
  void addByDocs({
    List<dynamic>? fetch,
    List<dynamic>? data,
    bool isRefresh = false,
  }) {
    if (isRefresh) {
      updateNoMoreByDocs(data);
      clearList();
    }
    updateNoMoreByDocs(data);

    if (isExists(data)) {
      for (var item in data!) {
        if (isExists(item)) {
          list.add(item);
        }
      }
    }
    if (isExists(fetch)) {
      lastSnapshot = fetch!.last;
    }
  }

  void refreshByDocs(List<dynamic>? fetch, List<dynamic>? data) {
    updateNoMoreByDocs(data);
    clearList();
    addByDocs(fetch: fetch, data: data);
  }

  void updateNoMoreByDocs(List<dynamic>? snapshot) {
    var isNoMore = isNotExists(snapshot) || isNotPositiveNum(snapshot!.length);
    if (isNoMore) {
      loadingState.noMore = true;
    }
  }

  ///
  /// By Maps
  ///
  void addByMaps({
    List<dynamic>? fetch,
    Map<String, dynamic>? data,
    bool isRefresh = false,
  }) {
    if (isRefresh) {
      updateNoMoreByMaps(data);
      clearList();
    }
    updateNoMoreByMaps(data);

    if (isExists(data)) {
      data?.forEach((key, value) {
        list.add(value);
      });
    }
    if (isExists(fetch)) {
      lastSnapshot = fetch!.last;
    }
  }

  void addByMapsWithExceptDelete({
    List<dynamic>? fetch,
    Map<String, dynamic>? data,
    bool isRefresh = false,
  }) {
    if (isRefresh) {
      updateNoMoreByMaps(data);
      clearList();
    }
    updateNoMoreByMaps(data);

    if (isExists(data)) {
      data?.forEach((key, value) {
        if (value.isDelete) {
          list.add(value);
        }
      });
    }
    if (isExists(fetch)) {
      lastSnapshot = fetch!.last;
    }
  }

  void addByMapsWithExceptUnsubscribe({
    List<dynamic>? fetch,
    Map<String, dynamic>? data,
    bool isRefresh = false,
  }) {
    if (isRefresh) {
      updateNoMoreByMaps(data);
      clearList();
    }
    updateNoMoreByMaps(data);

    if (isExists(data)) {
      data?.forEach((key, value) {
        if (value.isSubscribe) {
          list.add(value);
        }
      });
    }
    if (isExists(fetch)) {
      lastSnapshot = fetch!.last;
    }
  }

  void refreshByMaps(List<dynamic>? fetch, Map<String, dynamic>? data) {
    updateNoMoreByMaps(data);
    clearList();
    addByMaps(fetch: fetch, data: data);
  }

  void updateNoMoreByMaps(Map<String, dynamic>? snapshot) {
    var isNoMore = isNotExists(snapshot) || isNotPositiveNum(snapshot!.length);
    if (isNoMore) {
      loadingState.noMore = true;
    }
  }

  void mergeListByOrder(List<dynamic>? inserts) {
    if (isExists(list) && isExists(inserts)) {
      for (final element in inserts!) {
        if (list.contains(element)) continue;

        final int order = element.order;
        final length = list.length;
        final insert = order < length ? order : length;
        if (insert >= 0) list.insert(insert, element);
      }
    }
  }

  ///
  /// Function
  ///
  void failedFetch() {
    loadingState.noMore = true;
  }

  void clearList() {
    list.clear();
    lastSnapshot = null;
  }

  void registerRefreshCallback(VoidCallback callback) {
    refreshCallback = callback;
  }

  void removeRefreshCallback(VoidCallback callback) {
    var isEqual = (refreshCallback == callback);
    if (isEqual) {
      refreshCallback = null;
    }
  }

  ///
  /// Get / Set
  ///
  int lengthOfList() {
    return list.length;
  }

  bool checkIsExistOfList() {
    return list.isNotEmpty;
  }

  bool checkIsNotExistOfList() {
    return list.isEmpty;
  }

  bool checkIsShowOfList() {
    return checkIsExistOfList() && loadingState.checkLoaded();
  }
}
