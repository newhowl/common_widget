import 'package:common_util/util/int_util.dart';
import 'package:common_util/util/list_util.dart';
import 'package:common_util/util/media_util.dart';
import 'package:common_util/util/object_util.dart';
import 'package:common_widget/widget/edge/edge_inset.dart';
import 'package:common_widget/widget/indicator/jumping_progress_indicator.dart';
import 'package:common_widget/widget/more_list_view/top_scroll_button.dart';
import 'package:common_widget/widget/more_list_view/top_scroll_constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'more_list_constant.dart';
import 'more_list_model.dart';
import 'top_scroll_model.dart';

class MoreListView extends StatefulWidget {
  const MoreListView({
    Key? key,
    this.isMoreNext = true,
    this.isTopButton = false,
    this.isShowLoading = false,
    this.reverse = false,
    this.physics,
    required this.moreFunction,
    required this.refreshFunction,
    required this.itemList,
    required this.itemBuilder,
    this.headerBuilder,
    this.footerBuilder,
    this.emptyBuilder,
    this.errorBuilder,
    this.padding,
    this.itemMargin,
    this.direction = Axis.vertical,
    this.widthOrg,
    this.heightOrg,
    this.loadingTopPaddingOrg = moreListLoadingTopPadding,
    this.firstTopPaddingOrg = moreListFirstTopPadding,
    this.footerSizeOrg = moreListFooterSize,
  }) : super(key: key);

  final bool isMoreNext;
  final bool isTopButton;
  final bool isShowLoading;
  final bool reverse;
  final ScrollPhysics? physics;
  final Function moreFunction;
  final Function refreshFunction;
  final MoreListModel itemList;
  final MoreListWidgetBuilder itemBuilder;
  final WidgetBuilder? headerBuilder;
  final WidgetBuilder? footerBuilder;
  final WidgetBuilder? emptyBuilder;
  final WidgetBuilder? errorBuilder;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? itemMargin;
  final Axis direction;
  final double? widthOrg;
  final double? heightOrg;
  final double loadingTopPaddingOrg;
  final double firstTopPaddingOrg;
  final double footerSizeOrg;

  @override
  _MoreListViewState createState() => _MoreListViewState();
}

class _MoreListViewState extends State<MoreListView> {
  Future<void> _loadMore(int index) async {
    var isNeedMore = checkNeedMore(index);
    if (!isNeedMore) return;

    var isNoMore = widget.itemList.loadingState.noMore;
    if (isNoMore) return;

    var isCall = await widget.moreFunction();
    if (isCall) {
      setState(() {});
    }
  }

  Future<void> _refresh() async {
    widget.itemList.loadingState.noMore = false;
    widget.itemList.lastSnapshot = null;
    var isCall = await widget.refreshFunction.call();
    if (isCall) {
      setState(() {});
    }
    return;
  }

  Future<void> _loadMoreFirst() async {
    await _loadMore(0);
  }

  Future<void> _loadMoreNext(int index) async {
    if (widget.isMoreNext) {
      await _loadMore(index);
    }
  }

  bool checkNeedMore(int index) {
    var count = widget.itemList.list.length;
    return (count <= index + 1);
  }

  dynamic getItemOfListByIndex(int index) {
    return getItemFromList(widget.itemList.list, index);
  }

  int get lengthOfList => widget.itemList.list.length;

  int get lengthOfHeader => (null != widget.headerBuilder) ? 1 : 0;

  int get lengthOfEmpty => 1;

  int get lengthOfError => 1;

  int get lengthOfLoading => 1;

  int get lengthOfFooter => 1;

  int get lengthOfListWithBuilder => lengthOfList + lengthOfHeader + lengthOfEmpty + lengthOfError + lengthOfLoading + lengthOfFooter;

  TopScroll get topScroll => widget.itemList.topScroll;

  ScrollController get listController => widget.itemList.topScroll.scrollController;

  @override
  void initState() {
    widget.itemList.registerRefreshCallback(_refresh);
    _loadMoreFirst();
    super.initState();
  }

  @override
  void dispose() {
    widget.itemList.removeRefreshCallback(_refresh);
    super.dispose();
  }

  @override
  void setState(fn) {
    if (!mounted) return;
    super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return _bodyWidget();
  }

  Widget _bodyWidget() {
    return Stack(
      children: [
        Positioned.fill(child: _listWidget()),
        _bottomWidget(),
      ],
    );
  }

  Widget _listWidget() {
    var isItem = widget.itemList.list.isNotEmpty;
    var physics = isItem
        ? const AlwaysScrollableScrollPhysics()
        : const NeverScrollableScrollPhysics();
    physics = const AlwaysScrollableScrollPhysics();
    var width = widget.widthOrg ?? widthMedia(context);
    var height = widget.heightOrg ?? heightMedia(context);


    return RefreshIndicator(
      onRefresh: _refresh,
      child: SizedBox(
        width: width,
        height: height,
        child: ListView.builder(
          physics: physics,
          padding: widget.padding ?? edgeAllScreenUtil(0.0),
          scrollDirection: widget.direction,
          shrinkWrap: true,
          controller: listController,
          reverse: widget.reverse,
          itemCount: lengthOfListWithBuilder,
          itemBuilder: (context, index) {
            _loadMoreNext(index);
            return _itemWidget(context, index);
          },
        ),
      ),
    );
  }

  Widget _itemWidget(BuildContext context, int index) {
    if (null != widget.headerBuilder && 0 == index) {
      return _itemHeaderWidget(context, index);
    } else if (lengthOfListWithBuilder == index + 1) {
      return _itemEmptyWidget(context);
    } else if (lengthOfListWithBuilder == index + 2) {
      return _itemErrorWidget(context);
    } else if (lengthOfListWithBuilder == index + 3) {
      return _itemLoadingWidget(context);
    } else if (lengthOfListWithBuilder == index + 4) {
      return _itemFooterWidget(context);
    } else {
      var isHeader = isExists(widget.headerBuilder);
      var revisionIndex = index - (isHeader ? 1 : 0);
      return _itemBodyWidget(context, revisionIndex);
    }
  }

  Widget _itemHeaderWidget(BuildContext context, int index) {
    return widget.headerBuilder!(context);
  }

  Widget _itemEmptyWidget(BuildContext context) {
    var isFooter = isExists(widget.emptyBuilder);
    var isLoaded = widget.itemList.loadingState.checkDone();
    var isItem = widget.itemList.list.isNotEmpty;
    return isFooter && isLoaded && !isItem
        ? widget.emptyBuilder!(context)
        : Container();
  }

  Widget _itemErrorWidget(BuildContext context) {
    var isFooter = isExists(widget.errorBuilder);
    var isError = widget.itemList.loadingState.checkError();
    var isItem = widget.itemList.list.isNotEmpty;
    return isFooter && isError && !isItem
        ? widget.errorBuilder!(context)
        : Container();
  }

  Widget _itemLoadingWidget(BuildContext context) {
    var isLoading = widget.itemList.loadingState.checkLoading();
    var isExistsItem = isPositiveNum(widget.itemList.lengthOfList());
    var isShowLoading = widget.isShowLoading && isLoading;
    var topPadding = isExistsItem ? widget.loadingTopPaddingOrg : widget.firstTopPaddingOrg;

    return isShowLoading
        ? Container(padding: edgeOnlyScreenUtil(topOrg: topPadding), child: const JumpingProgressIndicator())
        : Container();
  }

  Widget _itemFooterWidget(BuildContext context) {
    var isFooter = isExists(widget.footerBuilder);

    return isFooter
        ? widget.footerBuilder!(context)
        : SizedBox(width: widget.footerSizeOrg, height: widget.footerSizeOrg,);
  }

  Widget _itemBodyWidget(BuildContext context, int index) {
    var item = getItemOfListByIndex(index);
    return Container(
      margin: widget.itemMargin,
      child: widget.itemBuilder(context, index, item),
    );
  }

  Widget _bottomWidget() {
    var isTopButton = widget.isTopButton && isPositiveNum(widget.itemList.lengthOfList());

    return isTopButton ? _scrollTopButton() : Container();
  }

  Positioned _scrollTopButton() {
    return Positioned(
      bottom: topScrollDimensionButtonPositionBottom,
      right: topScrollDimensionButtonPositionRight,
      child: _topButtonWidget(),
    );
  }

  Widget _topButtonWidget() {
    return TopScrollButton(topScroll: topScroll, key: null,);
  }
}