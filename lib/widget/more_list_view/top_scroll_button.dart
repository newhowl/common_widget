import 'package:common_util/constant/color_constant.dart';
import 'package:common_widget/widget/asset_safe_image/asset_safe_image.dart';
import 'package:common_widget/widget/button/ink_well_button.dart';
import 'package:common_widget/widget/edge/edge_inset.dart';
import 'package:common_widget/widget/more_list_view/top_scroll_constant.dart';
import 'package:common_widget/widget/more_list_view/top_scroll_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TopScrollButton extends StatefulWidget {
  const TopScrollButton({
    required Key? key,
    required this.topScroll,
  }) : super(key: key);

  final TopScroll topScroll;

  @override
  _TopScrollButtonState createState() => _TopScrollButtonState();
}

class _TopScrollButtonState extends State<TopScrollButton> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void setState(VoidCallback fn) {
    if (!mounted) return;
    super.setState(fn);
  }

  void refreshView() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    widget.topScroll.refreshTopButtonCallback = refreshView;
    return _topButtonWidget();
  }

  Widget _topButtonWidget() {
    return Container(
      width: topScrollDimensionButtonSize,
      height: topScrollDimensionButtonSize,
      padding: edgeAllScreenUtil(5),
      child: _animatedWidget(),
    );
  }

  Widget _animatedWidget() {
    final isOpacity = widget.topScroll.isTopButtonOpacity;

    return AnimatedOpacity(
      opacity: isOpacity ? 1 : 0,
      duration: const Duration(milliseconds: topScrollButtonOpacityDuration),
      child: _topButtonIcon(),
    );
  }

  Widget _topButtonIcon() {
    final isShow = widget.topScroll.isTopButtonShow;

    return isShow ? _topButtonShowIcon() : Container();
  }

  Widget _topButtonShowIcon() {
    return Container(
      decoration: BoxDecoration(
        color: colorWhite,
        boxShadow: [
          BoxShadow(
            offset: const Offset(0.0, 1.0),
            blurRadius: 1.0,
            spreadRadius: 1.5,
            color: Colors.black.withOpacity(0.15),
          ),
        ],
        shape: BoxShape.circle,
        border: Border.all(
          width: 0.5,
          color: colorBlueA1ACC0,
        ),
      ),
      child: InkWellButton(
        focusColor: colorClear,
        highlightColor: colorClear,
        splashColor: colorClear,
        onTap: () {
          if (widget.topScroll.isTopButtonShow) {
            widget.topScroll.moveTopListScroller?.call();
          }
        },
        child: const Center(
          child: SizedBox(
            width: topScrollDimensionButtonAssetSize,
            height: topScrollDimensionButtonAssetSize,
            child: AssetSafeImage(
              asset: topScrollAssetIconButtonAlignTop,
              color: colorBlack,
            ),
          ),
        ),
      ),
    );
  }
}
