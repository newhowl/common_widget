///
/// Duration
///
const int topScrollButtonPeriodicDuration = 700;
const int topScrollButtonSustainDuration = 1500;
const int topScrollButtonOpacityDuration = 500;

///
/// Dimension
///
const double topScrollDimensionButtonHeight = 100.0;
const double topScrollDimensionButtonSize = 50.0;
const double topScrollDimensionButtonAssetSize = 24.0;
const double topScrollDimensionButtonPositionBottom = 16.0;
const double topScrollDimensionButtonPositionRight = 8.0;

///
/// Asset
///
const String topScrollAssetIconButtonAlignTop = 'assets/image/common/icon_common_top.png';

