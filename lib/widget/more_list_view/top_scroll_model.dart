// top button
import 'dart:async';

import 'package:common_util/util/date_util.dart';
import 'package:common_util/util/object_util.dart';
import 'package:common_widget/widget/more_list_view/top_scroll_constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class TopScroll {
  ScrollController scrollController = ScrollController();
  bool isListenerTopButton = false;
  bool isTopButtonShow = false;
  bool isTopButtonOpacity = false;
  bool isListener = false;
  int topButtonShowMilliSecond = 0;
  double upOffset = 0.0;
  double currentOffset = 0.0;
  VoidCallback? refreshTopButtonCallback;
  Function? moveTopListScroller;

  void dispose() {
    scrollController.removeListener(onAddListener);
    scrollController.dispose();
  }

  /*
  탑버튼 리스너 등록.
   */
  void addListenerScrollControllerForTopButton() {
    scrollController.addListener(onAddListener);
  }

  void onAddListener() {
    var isFullUpOffset = false;

    if (scrollController.position.userScrollDirection == ScrollDirection.forward) {
      upOffset += currentOffset - scrollController.offset;
      currentOffset = scrollController.offset;
      if (topScrollDimensionButtonHeight < upOffset) {
        isFullUpOffset = true;
      }
    } else if (scrollController.position.userScrollDirection == ScrollDirection.reverse) {
      upOffset = 0.0;
      currentOffset = scrollController.offset;
    }

    if (isFullUpOffset) {
      topButtonShowMilliSecond = dateTimeNow();
      isTopButtonOpacity = true;
      refreshTopButtonCallback?.call();
      if (isTopButtonShow == false) {
        isTopButtonShow = true;
        Timer.periodic(const Duration(milliseconds: topScrollButtonPeriodicDuration), (timer) {
          if (topButtonShowMilliSecond + topScrollButtonOpacityDuration < dateTimeNow()) {
            isTopButtonOpacity = false;
            refreshTopButtonCallback?.call();
          }
          if (topButtonShowMilliSecond + topScrollButtonSustainDuration < dateTimeNow()) {
            isTopButtonShow = false;
            isTopButtonOpacity = false;
            refreshTopButtonCallback?.call();
            timer.cancel();
          }
        });
      }
    }
  }

  void jumpTo(double position) {
    if (isExists(scrollController) && scrollController.hasClients) {
      scrollController.jumpTo(0.0);
    }
  }
}
