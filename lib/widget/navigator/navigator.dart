import 'package:common_util/util/object_util.dart';
import 'package:flutter/material.dart';

Future<T?> navigatorByPage<T>(
  BuildContext context,
  Widget widget, {
  bool withName = false,
  String? name,
  bool fullscreenDialog = false,
}) async {
  return Navigator.push(
    context,
    MaterialPageRoute(
      settings: RouteSettings(name: name),
      fullscreenDialog: fullscreenDialog,
      builder: (context) => widget,
    ),
  );
}

Future<T?> navigatorByDialog<T>(
  BuildContext context,
  Widget widget, {
  bool withName = false,
  String? name,
}) async {
  return Navigator.of(context).push(PageRouteBuilder<T>(
    opaque: false,
    settings: RouteSettings(name: name),
    pageBuilder: (BuildContext context, _, __) {
      return widget;
    },
  ));
}

void navigatorPop(BuildContext context, {dynamic param}) {
  if (isExists(param)) {
    Navigator.of(context).pop(param);
  } else {
    Navigator.of(context).pop();
  }
}
