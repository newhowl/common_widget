import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'network_image_view_app.dart';
import 'network_image_view_web.dart';

class NetworkImageView extends StatelessWidget {
  const NetworkImageView({
    Key ? key,
    required this.imageUrl,
    this.width,
    this.height,
    this.fit = BoxFit.cover,
    this.errorWidget,
  }) : super(key: key);

  final String imageUrl;
  final double? width;
  final double? height;
  final BoxFit? fit;
  final Widget? errorWidget;

  @override
  Widget build(BuildContext context) {
    return kIsWeb ? _web(context) : _app(context);
  }

  Widget _web(BuildContext context) {
    return NetworkImageViewForWeb(
      imageUrl: imageUrl,
      width: width,
      height: height,
      fit: fit,
      errorWidget: errorWidget,
    );
  }

  Widget _app(BuildContext context) {
    return NetworkImageViewForApp(
      imageUrl: imageUrl,
      width: width,
      height: height,
      fit: fit,
      errorWidget: errorWidget,
    );
  }
}