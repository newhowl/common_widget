import 'package:cached_network_image/cached_network_image.dart';
import 'package:common_util/util/string_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NetworkImageViewForApp extends StatelessWidget {
  const NetworkImageViewForApp({
    Key? key,
    required this.imageUrl,
    this.width,
    this.height,
    this.fit = BoxFit.cover,
    this.errorWidget,
  }) : super(key: key);

  final String imageUrl;
  final double? width;
  final double? height;
  final BoxFit? fit;
  final Widget? errorWidget;

  @override
  Widget build(BuildContext context) {
    var isImageUrl = isExistsStr(imageUrl);

    return isImageUrl ? _existsImage() : _notExistsImage();
  }

  Widget _existsImage() {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      width: width,
      height: height,
      fit: fit,
      placeholder: _placeholder,
      errorWidget: _errorWidget,
    );
  }

  Widget _notExistsImage() {
    return errorWidget ??
        SizedBox(
          width: width,
          height: height,
        );
  }

  Widget _placeholder(BuildContext context, String url) {
    return Container();
  }

  Widget _errorWidget(BuildContext context, String url, dynamic error) {
    return Container();
  }
}
