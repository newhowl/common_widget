import 'package:common_util/constant/color_constant.dart';
import 'package:common_util/util/object_util.dart';
import 'package:flutter/cupertino.dart';

class NetworkImageViewForWeb extends StatelessWidget {
  const NetworkImageViewForWeb({
    Key ? key,
    required this.imageUrl,
    this.width,
    this.height,
    this.fit = BoxFit.cover,
    this.errorWidget,
  }) : super(key: key);

  final String imageUrl;
  final double? width;
  final double? height;
  final BoxFit? fit;
  final Widget? errorWidget;

  @override
  Widget build(BuildContext context) {
    return Image.network(
      imageUrl,
      width: width,
      height: height,
      fit: fit,
      errorBuilder: (context, error, stackTrace) {
        return _errorWidget();
      },
    );
  }

  Widget _errorWidget() {
    return Container(
      width: width,
      height: height,
      alignment: Alignment.center,
      child: isExists(errorWidget) ? errorWidget : Container(color: colorWhite,),
    );
  }
}