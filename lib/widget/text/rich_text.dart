import 'package:common_util/constant/color_constant.dart';
import 'package:common_util/constant/text_size_constant.dart';
import 'package:common_util/util/object_util.dart';
import 'package:common_util/util/string_util.dart';
import 'package:common_widget/widget/text/text_style.dart';
import 'package:flutter/material.dart';

Widget richTextOfString(
    String str, {
      int? maxLines,
      int? minLines,
      double size = textSize12,
      double? height,
      Color color = colorBlack,
      bool softWrap = true,
      TextOverflow overflow = TextOverflow.ellipsis,
      TextAlign textAlign = TextAlign.start,
      String? fontFamily,
      FontWeight fontWeight = normalFontWeight,
    }) {
  var text = isExistsStr(str) ? str : ' ';

  if (isExists(minLines)) {
    for (int index = 0; index < minLines!; index++) {
      text += '\n';
    }
  }

  return RichText(
    maxLines: maxLines,
    softWrap: softWrap,
    overflow: overflow,
    textAlign: textAlign,
    text: TextSpan(
      text: text,
      style: textStyleForm(
        size: size,
        color: color,
        height: height,
        fontFamily: fontFamily,
        fontWeight: fontWeight,
      ),
    ),
  );
}

Widget richTextOfMultiString({
  int? maxLines,
  bool softWrap = true,
  TextOverflow overflow = TextOverflow.ellipsis,
  TextAlign textAlign = TextAlign.start,
  required List<TextSpan> spans,
}) {
  var isSpan = spans.isNotEmpty;
  if (!isSpan) return Container();

  return RichText(
    maxLines: maxLines,
    softWrap: softWrap,
    overflow: overflow,
    textAlign: textAlign,
    text: TextSpan(
      children: [
        for (var item in spans) item,
      ],
    ),
  );
}

TextSpan richTextSpan(str, {
  double size = textSize12,
  Color color = colorBlack,
  String? fontFamily,
  FontWeight fontWeight = normalFontWeight,
}) {
  var style = textStyleForm(
    size: size,
    color: color,
    fontFamily: fontFamily,
    fontWeight: fontWeight,
  );
  var text = isExistsStr(str) ? str : ' ';
  return TextSpan(text: text, style: style,);
}