import 'package:common_util/constant/text_size_constant.dart';
import 'package:flutter/material.dart';

const FontWeight lightFontWeight = FontWeight.w300;
const FontWeight normalFontWeight = FontWeight.w500;
const FontWeight boldFontWeight = FontWeight.w700;

TextStyle textStyleForm({
  double size = textSize12,
  double? height,
  double wordSpacingOrg = 0.0,
  double letterSpacingOrg = -0.5,
  Color color = Colors.white,
  String? fontFamily,
  FontWeight fontWeight = normalFontWeight,
  TextDecoration? decoration,
}) {
  return TextStyle(
    color: color,
    height: height,
    wordSpacing: wordSpacingOrg,
    letterSpacing: letterSpacingOrg,
    fontSize: size,
    fontFamily: fontFamily,
    fontWeight: fontWeight,
    fontStyle: FontStyle.normal,
    decoration: decoration,
  );
}
