import 'package:common_widget/widget/toast/toast_view_app.dart';
import 'package:common_widget/widget/toast/toast_view_web.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:fluttertoast/fluttertoast.dart';

Future<void> toastView({
  required String message,
  /// for app
  BuildContext? context,
  StyledToastPosition position = StyledToastPosition.top,
  Duration duration = const Duration(milliseconds: 4000),
  /// for web
  var toastLength = Toast.LENGTH_SHORT,
  var gravity = ToastGravity.CENTER,
  var timeInSecForIosWeb = 3,
  var backgroundColor = Colors.black,
  var textColor = Colors.white,
  var webPosition = 'center',
  var webShowClose = true,
  var fontSize = 16.0,
  var isError = false,
}) async {
  return kIsWeb ?
  toastViewForWeb(
    message: message,
    toastLength: toastLength,
    gravity: gravity,
    timeInSecForIosWeb: timeInSecForIosWeb,
    backgroundColor: backgroundColor,
    textColor: textColor,
    webPosition: webPosition,
    webShowClose: webShowClose,
    fontSize: fontSize,
    isError: isError,
  ) :
  toastViewForApp(
    message: message,
    context: context,
    position: position,
    duration: duration,
  );
}