import 'package:common_util/constant/color_constant.dart';
import 'package:common_util/constant/text_size_constant.dart';
import 'package:common_util/util/color_util.dart';
import 'package:common_util/util/object_util.dart';
import 'package:common_widget/widget/edge/edge_inset.dart';
import 'package:common_widget/widget/text/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';

void toastViewForApp({
  required String message,
  BuildContext? context,
  StyledToastPosition position = StyledToastPosition.top,
  Duration duration = const Duration(milliseconds: 4000),
}) {
  if (isNotExists(context)) return;

  showToast(
    message,
    context: context,
    position: position,
    backgroundColor: colorOpacity(colorBlack, 0.8),
    textStyle: textStyleForm(
      size: textSize14,
      color: colorWhite,
      fontWeight: boldFontWeight,
    ),
    shapeBorder: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(8.0),
      ),
      side: BorderSide(color: colorWhite),
    ),
    duration: duration,
    textPadding: edgeAllScreenUtil(16.0),
  );
}