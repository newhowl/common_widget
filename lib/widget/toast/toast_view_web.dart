import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

Future<void> toastViewForWeb({
  var message = '',
  var toastLength = Toast.LENGTH_SHORT,
  var gravity = ToastGravity.CENTER,
  var timeInSecForIosWeb = 3,
  var backgroundColor = Colors.black,
  var textColor = Colors.white,
  var webPosition = 'center',
  var webShowClose = true,
  var fontSize = 16.0,
  var isError = false,
}) async {
  var webBgColor = "linear-gradient(to right, #00b09b, #96c93d)";
  if(isError) {
    webBgColor = "linear-gradient(to right, #f5a623, #FF0000)";
  }

  Fluttertoast.showToast(
    msg: message,
    toastLength: toastLength,
    gravity: gravity,
    timeInSecForIosWeb: timeInSecForIosWeb,
    backgroundColor: backgroundColor,
    textColor: textColor,
    webPosition: webPosition,
    webShowClose: webShowClose,
    webBgColor: webBgColor,
    fontSize: fontSize,
  );
}
